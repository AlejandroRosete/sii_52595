// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	char cadena[100];
		
	//Cerrar la tubería adecuadamente
	close (fd);
	unlink("/tmp/FIFO");
	unlink("/tmp/Bot.txt");

	munmap(proyeccion_memoria,sizeof(cadena));
	

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	for(i=0;i<esferas.size();i++)
		esferas[i].Dibuja();
	//for(i=0;i<esferasCentral.size();i++)
	//	esferasCentral[i].Dibuja();
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	//esfera.Mueve(0.025f);
	int i, j;
	for(i=0;i<esferas.size();i++)
		esferas[i].Mueve(0.025f);
	for(i=0;i<paredes.size();i++)
	{	
		for(j=0;j<esferas.size();j++)
			paredes[i].Rebota(esferas[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	for(i=0;i<limite.size();i++)
	{
		limite[i].Rebota(jugador1);
		limite[i].Rebota(jugador2);
	}

	for(int i=0;i<esferas.size();i++){

		jugador1.Rebota(esferas[i]);
		jugador2.Rebota(esferas[i]);
		if(fondo_izq.Rebota(esferas[i]))
		{
			esferas[i].radio=0.75f;
			esferas[i].centro.x=0.0f;
			//esferas[i].centro.y=rand()/(float)RAND_MAX;
			esferas[i].centro.y=0.0f;
			//printf("El nuevo centro de la esfera %d ha cambiado %f %f\n", i, esferas[i].centro.y, esferas[i].centro.x);
			esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i].velocidad.y=1+2*rand()/(float)RAND_MAX;
			puntos2++;

			//Práctica3: FIFO
			char mensaje2[100]="Jugador 2 marca 1 punto, lleva un total de ";
			char buffer2[10];		
			sprintf(buffer2, "%d",puntos2);
			strcat(mensaje2,buffer2);
			strcat(mensaje2, " puntos\n");
			if (write(fd,mensaje2, strlen(mensaje2)*sizeof(char))==-1)
				perror("Error en la escritura en el fifo de los puntos del jugador 2\n");
		}

		if(fondo_dcho.Rebota(esferas[i]))
		{
			esferas[i].radio=0.75f;
			esferas[i].centro.x=0.0f;
			//esferas[i].centro.y=rand()/(float)RAND_MAX;
			esferas[i].centro.y=0.0f;
			//printf("El nuevo centro de la esfera %d ha cambiado %f %f\n", i, esferas[i].centro.y, esferas[i].centro.x);
			esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esferas[i].velocidad.y=-1-2*rand()/(float)RAND_MAX;
			puntos1++;

			//Práctica3: FIFO
			char mensaje[100]="Jugador 1 marca 1 punto, lleva un total de ";
			char buffer1[10];		
			sprintf(buffer1, "%d",puntos1);
			strcat(mensaje,buffer1);
			strcat(mensaje, " puntos\n");
			if (write(fd,mensaje, strlen(mensaje)*sizeof(char)) == -1)
				perror("Error en la escritura en el fifo de los puntos del jugador 1\n");
		}
	} 

	if(esferas.size()<2 && puntos2>2*esferas.size() || puntos1>2*esferas.size()){
		Esfera e;
		esferas.push_back(e);
	}
	
	mem->esfera=esferas[0];
	mem->raqueta1=jugador2;	
	mem->raqueta2=jugador1;

	unsigned char keyword1, keyword2;
	if(mem->accion==-1){
		keyword1='k';
	}
	else if(mem->accion==1){
		keyword1='i';
	}
	else{
		keyword1='n';
	}
	if(mem->accion_raq2==-1){
		keyword2='s';
	}
	else if(mem->accion_raq2==1){
		keyword2='w';
	}
	else{
		keyword2='n';
	}
	CMundo::OnKeyboardDown(keyword1,0,0);
	CMundo::OnKeyboardDown(keyword2,0,0);

	//Bot que se mueve a los 10 segundos de inactividad:
	unsigned char key2;
	float contador = 0;
	CMundo::OnKeyboardDown(key2,0,0);
	if(contador < 10.0f){
		contador += 0.025f;
	}
	else{
		if(key2=='a'||key2=='d'||key2=='s'||key2=='w')
			contador = 0;
		else
			if(esferas[0].centro.y > jugador1.y1)
				key2='w';
			else if(esferas[0].centro.y == jugador1.y1)
				key2='q';
			else
				key2='s';
	}
	CMundo::OnKeyboardDown(key2,0,0);
		
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	Esfera esf1, esf2;
	switch(key)
	{
	case 'a':jugador1.velocidad.x=-2;break;
	case 'd':jugador1.velocidad.x=2;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'q':jugador1.velocidad.x = 0, jugador1.velocidad.y = 0;break;
	case 'j':jugador2.velocidad.x=-2;break;
	case 'l':jugador2.velocidad.x=2;break;
	case 'k':jugador2.velocidad.y=-4;break;
	case 'i':jugador2.velocidad.y=4;break;
	case 'u':jugador2.velocidad.x = 0, jugador2.velocidad.y = 0;break;

	case 'e':
		esf1.centro.x=jugador1.x1 + 1;
		esf1.centro.y=jugador1.y1;
		//esf1.Dibuja();		
		//esf1.Mueve(0.025f);
		esferas.push_back(esf1);
	break;
	case 'p':
		esf2.centro.x=jugador2.x1 - 1;
		esf2.centro.y=jugador2.y1;
		esf2.velocidad.x *= -1;
		//esf2.Dibuja();		
		//esf2.Mueve(0.025f);
		esferas.push_back(esf2);
	break;
	}
}

void CMundo::Init()
{

//Practica3: abrir el fifo en modo escritura
	int fd_escritura = open("/tmp/FIFO", O_WRONLY);
	if(fd_escritura == -1){
		perror("Error al abrir FIFO en modo escritura\n");
		exit(0);
	}
	
//Memoria compartida
	int fd_memComp = open("/tmp/Bot.txt",O_RDWR|O_CREAT|O_TRUNC,0666);
	if (fd_memComp == -1){
		perror("Error en el fichero de la memoria compartida.\n");
		exit(0);
	}
	//Para que empiece desde el principio
	ftruncate(fd_memComp,sizeof(memoria));

	//Proyección en memoria
	memoria_mmap=(char*)mmap(NULL,sizeof(memoria),PROT_WRITE|PROT_READ,MAP_SHARED,fd_memComp,0);

	//Cerrar el descriptor de fichero	
	close(fd_memComp);

	//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero a DatosMemCompartida, mem
	mem=(DatosMemCompartida*) memoria_mmap;

	int acc=0;
	


	Esfera aux;
	esferas.push_back(aux);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	p.x1=-7; p.y1=-5;
	p.x2=-7; p.y2=5;
	paredes.push_back(p);

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	p.x1=7; p.y1=-5;
	p.x2=7; p.y2=5;
	paredes.push_back(p);

//Central
	p.x1=0; p.y1=-5;
	p.x2=0; p.y2=5;
	limite.push_back(p);

}
