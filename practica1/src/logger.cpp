#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

int main(void){
	int fd;
	char cadena[100];
	int condicion_lectura = 0;
	
	//Crear el fifo
	if (mkfifo("/tmp/FIFO", 0777) == -1){
		perror("Error en el mkfifo");
		return 1;
	}

	//Abrir el fifo
	fd = open("/tmp/FIFO", O_RDONLY);
	if (fd == -1){
		perror("Error en el open del fifo");
		return 1;
	}

	//Bucle de lectura de datos
	while(condicion_lectura == 0){
		if(read(fd, cadena, sizeof(cadena))==0){
			printf("No hay datos que leer\n");
			condicion_lectura = 1;
		}
		else if(cadena[0]=='0')
			condicion_lectura = 1;
		else
			printf("%s",cadena);
	}

	if (condicion_lectura == 1)
		exit(1);
	//Destrucción
	unlink("/tmp/FIFO");
	close(fd);
	

}
