#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

#include "../include/DatosMemCompartida.h"
#include "../include/Esfera.h"
#include "../include/Raqueta.h"

int main(void){
	
	int fd_bot;
	char * proyeccion_memoria;
	
	//Crear un puntero a MemoriaCompartida
	DatosMemCompartida *memoria_bot;

	//Abrir fichero
	fd_bot = open("/tmp/Bot.txt", O_RDWR);
	if (fd_bot == -1){
		perror("Error al abrir el Bot.txt.\n");
		return 1;
	}
	
	ftruncate(fd_bot,sizeof(DatosMemCompartida));

	//Proyectar con mmap
	proyeccion_memoria = (char *)mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE|PROT_READ,MAP_SHARED,fd_bot,0);

	//Cerrar el fd
	close(fd_bot);

	//Asignar la dirección de comienzo
	memoria_bot = (DatosMemCompartida*)proyeccion_memoria;

	//Algoritmo para que el jugador (controlado por el bot) siga a la pelota
	while(1){
		usleep(25000);
		if(memoria_bot->esfera.centro.y<(memoria_bot->raqueta1.y2+memoria_bot->raqueta1.y1)/2)
			memoria_bot->accion=-1;
		else if(memoria_bot->esfera.centro.y>(memoria_bot->raqueta1.y2+memoria_bot->raqueta1.y1)/2)
			memoria_bot->accion=1;
		else
			memoria_bot->accion=0;

		usleep(100000);
		if(memoria_bot->esfera.centro.y<(memoria_bot->raqueta2.y2+memoria_bot->raqueta2.y1)/2)
			memoria_bot->accion_raq2=-1;
		else if(memoria_bot->esfera.centro.y>(memoria_bot->raqueta2.y2+memoria_bot->raqueta2.y1)/2)
			memoria_bot->accion_raq2=1;
		else
			memoria_bot->accion_raq2=0;

	}


	unlink ("/tmp/Bot.txt");

	munmap(proyeccion_memoria,sizeof(*(memoria_bot)));
}
