// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "DatosMemCompartida.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

using namespace std;

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	//Esfera esfera;
	std::vector<Plano> paredes;
	std::vector<Plano> limite;
	std::vector<Esfera> esferas;
	//std::vector<Esfera> esferasCentral;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	//Añadir el identificador de tubería
	string logger;

	//Añadir los atributos de DatosMemoriaCompartida
	DatosMemCompartida memoria;
	DatosMemCompartida *mem;
	char * memoria_mmap;

	int puntos1;
	int puntos2;

	char *proyeccion_memoria;

	int fd;
	int fdmem;
 	char * nomFifo;
	

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
