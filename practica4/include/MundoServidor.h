<<<<<<< HEAD
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "DatosMemCompartida.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	//Esfera esfera;
	std::vector<Plano> paredes;
	std::vector<Plano> limite;
	std::vector<Esfera> esferas;
	//std::vector<Esfera> esferasCentral;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	int fd;
	int fdmem;
 	char * nomFifo;
	DatosMemCompartida memoria;
	DatosMemCompartida *mem;


//Práctica 4:
	int fd4;
	char cad[100];

	int fdServidor;
	char cad_teclas[100];

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
=======
// MundoServidor.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "DatosMemCompartida.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

using namespace std;

#include <pthread.h>

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	std::vector<Plano> paredes;
	std::vector<Plano> limite;
	std::vector<Esfera> esferas;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	//Añadir los atributos de DatosMemoriaCompartida
	DatosMemCompartida memoria;
	DatosMemCompartida *mem;
	char * memoria_mmap;

	int puntos1;
	int puntos2;

	char *proyeccion_memoria;

	int fd;
	int fd_memComp;
	
	//Práctica 4
	int fd_serv_a_cliente;
	int fd_cliente_a_serv;

	int fd_escritura;

	pthread_t thid1;

	void RecibeComandosJugador();

};



#endif // !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
>>>>>>> practica4
